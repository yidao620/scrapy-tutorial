﻿# Python网络爬虫Scrapy框架研究

## Wiki
Scrapy是Python开发的一个快速,高层次的屏幕抓取和web抓取框架，用于抓取web站点并从页面中提取结构化的数据。
Scrapy用途广泛，可以用于数据挖掘、监测和自动化测试。

Scrapy吸引人的地方在于它是一个框架，任何人都可以根据需求方便的修改。它也提供了多种类型爬虫的基类，
如BaseSpider、sitemap爬虫等，还有对web2.0爬虫的支持。

Scrach是抓取的意思，这个Python的爬虫框架叫Scrapy，大概也是这个意思吧，就叫它：小刮刮吧。

基于最新的Scrapy 1.0编写，已更新至Python3.6

------------------------------------------

## Scrapy1.0教程

* [Scrapy笔记（1）- 入门篇](https://www.xncoding.com/2016/03/08/scrapy-01.html)
* [Scrapy笔记（2）- 完整示例](https://www.xncoding.com/2016/03/10/scrapy-02.html)
* [Scrapy笔记（3）- Spider详解](https://www.xncoding.com/2016/03/12/scrapy-03.html)
* [Scrapy笔记（4）- Selector详解](https://www.xncoding.com/2016/03/14/scrapy-04.html)
* [Scrapy笔记（5）- Item详解](https://www.xncoding.com/2016/03/16/scrapy-05.html)
* [Scrapy笔记（6）- Item Pipeline](https://www.xncoding.com/2016/03/18/scrapy-06.html)
* [Scrapy笔记（7）- 内置服务](https://www.xncoding.com/2016/03/19/scrapy-07.html)
* [Scrapy笔记（8）- 文件与图片](https://www.xncoding.com/2016/03/20/scrapy-08.html)
* [Scrapy笔记（9）- 部署](https://www.xncoding.com/2016/03/21/scrapy-09.html)
* [Scrapy笔记（10）- 动态配置爬虫](https://www.xncoding.com/2016/04/10/scrapy-10.html)
* [Scrapy笔记（11）- 模拟登录](https://www.xncoding.com/2016/04/12/scrapy-11.html)
* [Scrapy笔记（12）- 抓取动态网站](https://www.xncoding.com/2016/04/15/scrapy-12.html)

## 对多个内容网站的采集，主要功能实现如下：

  * 最新文章列表的爬取
  * 采集的数据放入MySQL数据库中，并且包含标题，发布日期，文章来源，链接地址等等信息
  * URL去重复，程序保证对于同一个链接不会爬取两次
  * 防止封IP策略，如果抓取太频繁了，就被被封IP，目前采用三种策略保证不会被封：

     * 策略1：设置download_delay下载延迟，数字设置为5秒，越大越安全
     * 策略2：禁止Cookie，某些网站会通过Cookie识别用户身份，禁用后使得服务器无法识别爬虫轨迹
     * 策略3：使用user agent池。也就是每次发送的时候随机从池中选择不一样的浏览器头信息，防止暴露爬虫身份
     * 策略4：使用IP池，这个需要大量的IP资源，貌似还达不到这个要求
     * 策略5：分布式爬取，这个是针对大型爬虫系统的，对目前而言我们还用不到。

  * 模拟登录后的爬取
  * 针对RSS源的爬取
  * 对于每个新的爬取目标网站，或者原来的网站格式有变动的时候，需要做到可配置，
    只修改配置文件即可，而不是修改源文件，增加一段爬虫代码，主要是用xpath配置爬取规则
  * 定时爬取，设置定时任务周期性爬取
  * 与微信公共平台的结合，给大量的订阅号随机分配最新的订阅文章。
  * 利用scrapy-splash执行页面javascript后的内容爬取

## Scrapy源码演示

web文件夹是静态html网站源文件。先开启一个简单HTTP服务器：

```
cd web/
python -m http.server 8000
```

### ch01 - 基础爬虫演示

* quotes01_spider：简单页面爬虫演示
* quotes02_spider：跟踪链接的爬取
* quotes03_spider：水平和垂直爬取

### ch02 - 手机APP演示

将数据爬取到https://appery.io，然后制作一个手机APP演示数据

```
pip install --upgrade scrapyapperyio
```
为了在python3下顺利运行，需要安装后修改源码scrapyapperyio.py，大概第21行改成：
```
try:
    from urllib import urlencode
except ImportError:
    from urllib.parse import urlencode
```

去网站https://scrapyappery.io注册账号，创建数据库scrapy，并新建集合mobile，拿到配置然后修改配置文件：
```
ITEM_PIPELINES = {
   'scrapyapperyio.ApperyIoPipeline': 300,
}
APPERYIO_DB_ID = '5cca7e2b0f0d3113cd9569fd'
APPERYIO_USERNAME = 'root'
APPERYIO_PASSWORD = 'pass'
APPERYIO_COLLECTION_NAME = 'mobile'
```

### ch03 - 登录爬虫演示

首先弄一个需要登录的网站。这里直接用[flaskr](https://github.com/pallets/flask/tree/master/examples/tutorial/)作为例子

本地安装好flaskr后，可通过`http://127.0.0.1:5000/`访问主页，如果要访问`http://127.0.0.1:5000/create`，
则需要登录，如果没有登录会跳转至登录页`http://127.0.0.1:5000/auth/login`

为了演示，先手动登录后发表几篇文章，然后通过爬虫将这些文章爬取下来。
登录爬虫除了能把首页文章爬取下来，还能进入新建文章的页面，将里面的内容也爬取下来。

### ch04 - 爬虫部署到scrapinghub

使用github登录https://scrapinghub.com/，然后创建一个project，复制API Key

配置scrapy.cfg

```
[settings]
default = ch04.settings

[deploy]
username = 9e4febf7651e45c3a4e5622d84e045b2
password = 
project = 389092
```

然后在统计目录下面新建scrapinghub.yml，配置如下
```
apikey: 9e4febf7651e45c3a4e5622d84e045b2
project: 389092
stacks:
    default: scrapy:1.6-py3
```

然后安装 `pip install shub`

登录：`shub login`，输入API key，如果上面已经配置过，则可以直接按Enter，然后执行
```
shub depoy
```

发布完成后，可以去scrapinghub后台，可以呀看到一个名字为hub的spider。然后创建一个新的Periodic Job，并指定爬虫名为hub即可。

不过定时任务爬虫需要订阅才能运行了，不过可以选择直接点击run来运行一次。或者在Spider上面直接点击Run也可以

![](ch04/hub.jpg)

运行完成后，可以选择将Items导出为csv、json等格式文件

------------------------------------------

## 贡献代码

1. Fork
1. 创建您的特性分支 git checkout -b my-new-feature
1. 提交您的改动 git commit -am 'Added some feature'
1. 将您的修改记录提交到远程 git 仓库 git push origin my-new-feature
1. 然后到 github 网站的该 git 远程仓库的 my-new-feature 分支下发起 Pull Request

## 许可证
Copyright (c) 2014-2016 [Xiong Neng](https://www.xncoding.com/)

基于 MIT 协议发布: <http://www.opensource.org/licenses/MIT>

