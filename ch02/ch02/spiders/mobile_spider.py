# -*- coding: utf-8 -*-

import scrapy

from ..items import MobileItem, MyItemLoader


class MobileSpider(scrapy.Spider):
    """
    简单页面爬虫演示
    """
    name = 'mobile'
    allowed_domains = ['localhost']
    start_urls = [
        'http://localhost:8000/page/page1.html'
    ]

    def parse(self, response):
        """
        爬取Quotes to Scrape首页并将数据导出到appery.io数据库中
        运行：scrapy crawl mobile -s CLOSESPIDER_ITEMCOUNT=90
        @url http://quotes.toscrape.com/
        @return items 10
        @scrapes text author tags
        :param response: 响应
        :return: 解析后的Item
        """
        for quote in response.xpath('//div[@class="col-md-8"]/div[@class="quote"]'):
            # Create the loader using the selector
            l = MyItemLoader(item=MobileItem(), selector=quote)

            # xpath
            l.add_xpath('text', './span[@class="text"]/text()')
            l.add_xpath('author', './span/small[@class="author"]/text()')
            l.add_xpath('tags', './div[@class="tags"]/a/text()')

            yield l.load_item()
