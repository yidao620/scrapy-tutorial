# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Join, MapCompose, TakeFirst, Compose
from w3lib.html import remove_tags

from scrapy.loader.processors import TakeFirst, MapCompose, Join


class MyItemLoader(ItemLoader):
    default_output_processor = TakeFirst()
    title_in = MapCompose(str.strip)
    author_in = Compose(MapCompose(str.title, str.strip),
                        lambda i: i[0].split()[1] if len(i[0].split()) > 2 else i[0])
    date_in = Compose(MapCompose(str.title, str.strip),
                      lambda i: i[0].split()[3] if len(i[0].split()) > 2 else i[0])


class LoginItem(scrapy.Item):
    # page field
    title = scrapy.Field()
    author = scrapy.Field()
    content = scrapy.Field()
    date = scrapy.Field()
