# -*- coding: utf-8 -*-
import scrapy


class Quotes02Spider(scrapy.Spider):
    """
    翻页爬虫演示。运行：scrapy crawl quotes02
    """
    name = 'quotes02'
    allowed_domains = ['localhost']
    start_urls = [
        'http://localhost:8000/page/page1.html'
    ]

    def parse(self, response):
        for quote in response.css('div.quote'):
            yield {
                'text': quote.css('span.text::text').get(),
                'author': quote.css('small.author::text').get(),
                'tags': quote.css('div.tags a.tag::text').getall(),
            }
        for href in response.css('li.next a::attr(href)'):
            yield response.follow(href, callback=self.parse)
