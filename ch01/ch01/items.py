# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Join, MapCompose, TakeFirst
from w3lib.html import remove_tags

from scrapy.loader.processors import TakeFirst, MapCompose, Join


class MyItemLoader(ItemLoader):
    default_output_processor = TakeFirst()

    text_in = MapCompose(str.strip)
    author_in = MapCompose(str.title, str.strip)
    tags_in = MapCompose(remove_tags)
    tags_out = Join(',')


class Ch01Item(scrapy.Item):
    # page field
    text = scrapy.Field()
    author = scrapy.Field()
    tags = scrapy.Field()

    # common filed
    url = scrapy.Field()
    project = scrapy.Field()
    spider = scrapy.Field()
    server = scrapy.Field()
    date = scrapy.Field()


class Ch03Item(scrapy.Item):
    # page field
    text = scrapy.Field()
    author = scrapy.Field()
    tags = scrapy.Field()
