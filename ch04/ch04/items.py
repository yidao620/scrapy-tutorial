# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Join, MapCompose, TakeFirst, Compose
from w3lib.html import remove_tags

from scrapy.loader.processors import TakeFirst, MapCompose, Join


class MyItemLoader(ItemLoader):
    # default_output_processor = TakeFirst()

    text_in = MapCompose(str.strip)
    author_in = MapCompose(str.title, str.strip)
    tags_in = MapCompose(remove_tags)
    # tags_out = MapCompose(Join(''))
    tags_out = Compose(Join(','), lambda i: [i])


class HubItem(scrapy.Item):
    # page field
    text = scrapy.Field()
    author = scrapy.Field()
    tags = scrapy.Field()
