# -*- coding: utf-8 -*-

import scrapy

from ..items import HubItem, MyItemLoader


class MobileSpider(scrapy.Spider):
    """
    爬虫部署至https://scrapinghub.com/，定期爬取
    """
    name = 'hub'
    allowed_domains = ['scrapy.xncoding.com']
    start_urls = [
        'http://scrapy.xncoding.com/page/page1.html'
    ]

    def parse(self, response):
        """
        :param response: 响应
        :return: 解析后的Item
        """
        for quote in response.xpath('//div[@class="col-md-8"]/div[@class="quote"]'):
            # Create the loader using the selector
            l = MyItemLoader(item=HubItem(), selector=quote)

            # xpath
            l.add_xpath('text', './span[@class="text"]/text()')
            l.add_xpath('author', './span/small[@class="author"]/text()')
            l.add_xpath('tags', './div[@class="tags"]/a/text()')

            yield l.load_item()
